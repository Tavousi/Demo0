# Persian Forum

---

![License](https://img.shields.io/badge/license-MIT-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/freshman/persian-forum.svg)](https://packagist.org/packages/freshman/persian-forum)

A [Flarum](http://flarum.org) extension. do some crazy

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require freshman/persian-forum
```

### Updating

```sh
composer update freshman/persian-forum
php flarum cache:clear
```

### Links

- [Packagist](https://packagist.org/packages/freshman/persian-forum)
